def printMatrix(matriz, n_linhas, n_colunas):
    for linhas in range(n_linhas):
        for colunas in range(n_colunas):
            print(matriz[linhas][colunas], end='\t')
        print()
    print("--------------")

def resolveEspiral(matriz):
    n_linhas = len(matriz)
    n_colunas = len(matriz[0])
    n_start = 0
    n_end = n_linhas
    m_start = 0
    m_end = n_colunas
    counter = 0
    elems = n_linhas*n_colunas

    while(n_end > n_start) or (m_end > m_start):
        if(m_end!=m_start):
            for m in range(m_start, m_end):
                matriz[n_start][m] = counter
                counter+= 1
                if elems == counter:
                    return
            #print(" >>> " + str(counter)  + " / " + str(elems))
            #printMatrix(matriz, n_linhas, n_colunas)
        if (n_start+1) != n_end:
            for n in range (n_start+1, n_end):
                matriz[n][m_end-1] = counter
                counter += 1
                if elems == counter:
                    return
            #print(" vvv " + str(counter)  + " / " + str(elems))
            #printMatrix(matriz, n_linhas, n_colunas)
        if (m_end-1) != m_start:
            for m in range(m_end-2, m_start-1, -1):
                matriz[n_end-1][m] = counter
                counter += 1
                if elems == counter:
                    return
            #print(" <<< " + str(counter)  + " / " + str(elems))
            #printMatrix(matriz, n_linhas, n_colunas)
        if (n_end-2) != n_start:
            for n in range(n_end-2, n_start, -1):
                matriz[n][m_start] = counter
                counter += 1
                if elems == counter:
                    return
            #print(" ^^^ " + str(counter)  + " / " + str(elems))
            #printMatrix(matriz, n_linhas, n_colunas)
        m_start += 1
        n_start += 1
        m_end   -= 1
        n_end   -= 1

def main():
    print("====================================================")
    print("Programa que faz espiral em matriz - aula 29/10/2019")
    print("=================COMP1 - DEL UFRJ===================")
    linhas = int(input("Entre com o numero de linhas:"))
    colunas = int(input("Entre com o numero de colunas:"))
    matriz = [0]*linhas
    for i in range(linhas):
        matriz[i] = [0]*colunas

    resolveEspiral(matriz)

    print("------- Final:")
    printMatrix(matriz, linhas, colunas)
    

main()