#############################################################
# QUESTAO 1
#############################################################
def leArquivo(arq):
    arq.seek(0,0)
    lista = arq.readlines()
    turma = []
    for line in lista:
        line = line[:-1] # remove o \n
        line_split = line.split(':')

        aluno = {'Nome':line_split[0], \
                 'Idade':int(line_split[1]), \
                 'Mae':line_split[2], \
                 'Irmaos':line_split[3:]}
        turma += [aluno]
    return turma

def main():

    arq = open("q1.txt", 'r')
    turma = leArquivo(arq)
    print(turma)

    return

main()