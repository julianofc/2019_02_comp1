#############################################################
# QUESTAO 2 (com idade)
#############################################################
def leArquivo(arq):
    arq.seek(0,0)
    lista = arq.readlines()
    turma = []
    for line in lista:
        line = line[:-1] # remove o \n
        line_split = line.split(':')

        aluno = {'Nome':line_split[0], \
                 'Idade':int(line_split[1]), \
                 'Mae':line_split[2], \
                 'Irmaos':[]}
        irmaos = line_split[3:]
        fill_name = True
        irmaos_split = {'Nome' : '', \
                        'Idade' : 0}
        for campo in irmaos:
            if fill_name == True:
                fill_name = False
                irmaos_split['Nome'] = campo
                
            else:
                fill_name = True
                irmaos_split['Idade'] = campo
                aluno['Irmaos'].append(irmaos_split.copy())      

        turma += [aluno]
    return turma

def main():

    arq = open("q1_with_age.txt", 'r')
    turma = leArquivo(arq)
    print(turma)
    return

main()