#############################################################
# QUESTAO 3 (com idade do irmão caçula)
#############################################################
def leArquivo(arq):
    arq.seek(0,0)
    lista = arq.readlines()
    turma = []
    for line in lista:
        line = line[:-1] # remove o \n
        line_split = line.split(':')

        aluno = {'Nome':line_split[0], \
                 'Idade':int(line_split[1]), \
                 'Mae':line_split[2], \
                 'Irmaos':[]}
        irmaos = line_split[3:]
        fill_name = True
        irmaos_split = {'Nome' : '', \
                        'Idade' : 0}
        for campo in irmaos:
            if fill_name == True:
                fill_name = False
                irmaos_split['Nome'] = campo
                
            else:
                fill_name = True
                irmaos_split['Idade'] = campo
                aluno['Irmaos'].append(irmaos_split.copy())      

        turma += [aluno]
    return turma

def acha_mais_novo(turma):

    for aluno in turma:
        cacula = {'Nome' : 'Thor', \
            'Idade' : 1039}
        #print(aluno['Irmaos'])
        for irmao in aluno['Irmaos']:
            if int(irmao['Idade']) < int(cacula['Idade']):
                cacula = irmao
        print("O aluno " + aluno['Nome'] + " possui o irmao mais novo " + str(cacula['Nome']))
        aluno['Cacula'] = cacula.copy()
    nome = ''
    return nome

def main():

    arq = open("q1_with_age.txt", 'r')
    turma = leArquivo(arq)

    acha_mais_novo(turma)
    print(turma)
    return

main()