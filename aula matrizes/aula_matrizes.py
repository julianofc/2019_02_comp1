m_lines = 6

matriz = [0]*m_lines

print("Matriz so com linhas: " + str(matriz))
print("--------------")

m_columns = 10
for i in range(m_lines):
    matriz[i] = [0]*m_columns

print("Matriz com linhas e colunas: " + str(matriz))
print("--------------")

print("****** Imprimindo matrizes ******")

print("Matriz tabulada:")
for linhas in range(m_lines):
    for colunas in range(m_columns):
        print(matriz[linhas][colunas], end='\t')
    print()
print("--------------")

matriz[2][2] = 3
print("Matriz com a posição [2][2] alterada:")
for linhas in range(m_lines):
    for colunas in range(m_columns):
        print(matriz[linhas][colunas], end='\t')
    print()
print("--------------")

