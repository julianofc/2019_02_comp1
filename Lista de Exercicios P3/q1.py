import os

theaterRows = "ABCDEFGHIJKLMNOPQRSTUVWX"

def printMatrix(matrix, n_lines, n_cols):
    print(" ", end='\t') #primeira letra
    for col in range(n_cols):
        print(col+1, end='\t')
    print()

    for line in range(n_lines):
        print(theaterRows[line] + " ", end='\t')
        for col in range(n_cols):
            if matrix[line][col]["ocupado"] == True:
                print("O", end='\t')
            else:
                print("L", end='\t')
        print(" " + theaterRows[line], end='\t')
        print()
    print("--------------")

def calculateTotalValue(seatMap):
    numRows = len(seatMap)
    numCols = len(seatMap[0])
    m_sum = 0.0
    for n in range(numRows):
        for m in range(numCols):
            if seatMap[n][m]["ocupado"] == True:
                m_sum += seatMap[n][m]["preco"]
    return m_sum



def buyOneTicket(seatMap, maxRow, maxCol):
    inputText = input("Entre com o assento escolhido:").upper()
    rowIndex = None
    if len(inputText) < 4:
        rowIndex = theaterRows.find(inputText[0])
        if (rowIndex == -1) or (rowIndex >= maxRow):
            return None
    else:
        return None

    colIndex = None
    if len(inputText) < 4:
        if inputText[1:].isnumeric():
            tempIndex = int(inputText[1:])
            if (tempIndex > 0) and (tempIndex <= maxCol):
                colIndex = tempIndex - 1
            else:
                return None
        else:
            return None
    else:
        return None

    if seatMap[rowIndex][colIndex]["ocupado"] == False:
        seatMap[rowIndex][colIndex]["nome"] = input("Entre com o nome do comprador:").upper()
        seatMap[rowIndex][colIndex]["ocupado"] = True
        return seatMap[rowIndex][colIndex]["preco"]

    return None

def buyTicket(seatMap, maxRow, maxCol):
    while True:
        validBuy = buyOneTicket(seatMap, maxRow, maxCol)
        if validBuy is not None:
            break
        else:
            print("Tente novamente. Assento ocupado ou entrada invalida")

    print("Compra efetuada com sucesso! Valor do ingresso: " + str(validBuy))

def createMatrix(rows, cols):
    matrix = [0]*rows
    for i in range(rows):
        matrix[i] = [0]*cols
    return matrix

def fillMatrix(matrix, rows, cols):
    elem = { "preco"  : 10.0,
            "ocupado": False,
            "nome"   : None
        }
    for n in range(rows):
        for m in range(cols):
            matrix[n][m] = elem.copy()

def saveMatrixToDisk(seatMap):
    numRows = len(seatMap)
    numCols = len(seatMap[0])
    cwd = os.getcwd()
    fileobj = open(cwd + "/seatmap.txt","w")

    for n in range(numRows):
        for m in range(numCols):
            if seatMap[n][m]["ocupado"] == True:
                hasPeople = "OCUPADO"
            else:
                hasPeople = "VAZIO"
            fileobj.write(theaterRows[n] + ":" + str(m+1) + ":" + hasPeople + \
                 ":" + str(seatMap[n][m]["preco"]) + ":" + str(seatMap[n][m]["nome"]) + "\n")

    fileobj.close()

def menu():
    while True:
        print("*****************************************************")
        print("    Sistema de compra da ingressos para um teatro    ")
        print("*****************************************************")
        print("Escolha a opção desejada:")
        print("(1) - Calcular o valor total das vendas")
        print("(2) - Comprar ingresso")
        print("(3) - Salvar dados em disco")
        print("(4) - Sair do programa")
        print("*****************************************************")
        inputOption = input()
        if inputOption[0].isdigit():
            menuRet = int(inputOption[0])
            if (menuRet < 5) and (menuRet > 0):
                return menuRet

        print("Opção inválida - tente novamente")

def main():
    rows = 24
    cols = 14
    matrix = createMatrix(rows,cols)
    fillMatrix(matrix, rows, cols)

    while True:
        menuOption = menu()
        
        if menuOption == 1:
            print("Valor total arrecadado foi de R$" + str(calculateTotalValue(matrix)))
        elif menuOption == 2:
            printMatrix(matrix,rows, cols)
            buyTicket(matrix,rows, cols)
            printMatrix(matrix,rows, cols)
        elif menuOption == 3:
            saveMatrixToDisk(matrix)
            print("Dados gravados em disco")
        else: 
            break

    print("Obrigado por usar o nosso software")    



if __name__ == "__main__":
    main()