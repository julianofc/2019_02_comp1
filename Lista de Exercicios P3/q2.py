def changeValues(A, B):
    A = A + B
    B = A - B
    A = A - B
    return A, B

def main():
    m_A = 3.5 #try different values
    m_B = 5.9 #try different values
    print("old A:" + str(m_A) + " old B:" + str(m_B))
    m_A, m_B = changeValues(m_A, m_B)

    print("new A:" + str(m_A) + " new B:" + str(m_B))

if __name__ == "__main__":
    main()