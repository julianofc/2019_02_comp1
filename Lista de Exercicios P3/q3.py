from q2 import changeValues as changeValues

def printMatrix(matriz, n_linhas, n_colunas):
    for linhas in range(n_linhas):
        for colunas in range(n_colunas):
            print(matriz[linhas][colunas], end='\t')
        print()
    print("--------------")

def calcDet(matrix):
    #determinante de matrizes somente 2x2
    return (matrix[0][0]*matrix[1][1])-(matrix[1][0]*matrix[0][1])

def invertMatrix(matrix):
    #inverter matrizes somente 2x2
    determ = calcDet(matrix)
    if determ != 0:
        print(matrix[0][0], matrix[1][1])
        matrix[0][0], matrix[1][1] = changeValues(matrix[0][0], matrix[1][1])
        #changeValues(matrix[0][0], matrix[1][1]) #testado em sala de aula - não funciona
        print(matrix[0][0], matrix[1][1])
        matrix[0][1] = -matrix[0][1]
        matrix[1][0] = -matrix[1][0]
        for n in range(len(matrix)):
            for m in range(len(matrix[0])):
                matrix[n][m] = matrix[n][m] / determ
        return matrix
    else:
        return None

def main():
    #cria uma matriz vazia
    m_size = 2
    matriz = [0]*m_size
    for i in range(m_size):
        matriz[i] = [0]*m_size

    matriz[0][0] = 1
    matriz[0][1] = 2
    matriz[1][0] = 3
    matriz[1][1] = 4

    printMatrix(matriz,m_size,m_size)
    matriz = invertMatrix(matriz) 
    if matriz != None:
        printMatrix(matriz,m_size,m_size)
    else:
        print("determinante = 0")
    
if __name__ == "__main__":
    main()