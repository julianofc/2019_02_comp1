def strToDictDate(myString):
    m_str = myString.split("/")
    m_Date = {"ANO": int(m_str[0]),\
              "MES": int(m_str[1]),\
              "DIA": int(m_str[2]) \
             }
    return m_Date

def strToBool(myString):
    if myString[len(myString)-1] == "\n":
        myString = myString[:-1]
    if myString == "SIM":
        return True
    return False

def strToList(myString):
    return myString.split(":")

def readFileToStrList(myFile):
    myFile.seek(0,0)
    return myFile.readlines()

def main():
    print("Programa de cadastro")
    patientDictList = []
    filePtr = open("cadastro.txt","r")
    fileLst = readFileToStrList(filePtr)
    for patient in fileLst:
        patientLst = strToList(patient)
        patientDict = { "CPF" : patientLst[0], \
                        "NOME": patientLst[1], \
                        "DATA_DE_NASCIMENTO": strToDictDate(patientLst[2]), \
                        "DATA_DO_CADASTRO": strToDictDate(patientLst[3]), \
                        "ATIVO": strToBool(patientLst[4])
        }
        patientDictList.append(patientDict)
    
    print(patientDictList)

main()