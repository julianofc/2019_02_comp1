def isCollisionDetected(rectOne, rectTwo):
    rectA = {"bottom"   : rectOne["bottomLeft"][0],
             "left"     : rectOne["bottomLeft"][1],
             "top"      : rectOne["topRight"][0],
             "right"    : rectOne["topRight"][1]
    }
    rectB = {"bottom"   : rectTwo["bottomLeft"][0],
             "left"     : rectTwo["bottomLeft"][1],
             "top"      : rectTwo["topRight"][0],
             "right"    : rectTwo["topRight"][1]
    }
    if  rectA["right"] < rectB["left"]   or \
        rectA["left"]  > rectB["right"]  or \
        rectA["top"]   < rectB["bottom"] or \
        rectA["bottom"] > rectB["top"]:
        return False
    
    return True

def main():
    BLx1 = float(input("Entre com o valor do vértice BLx do triangulo 1:"))
    BLy1 = float(input("Entre com o valor do vértice BLy do triangulo 1:"))
    TRx1 = float(input("Entre com o valor do vértice TRx do triangulo 1:"))
    TRy1 = float(input("Entre com o valor do vértice TRy do triangulo 1:"))

    BLx2 = float(input("Entre com o valor do vértice BLx do triangulo 2:"))
    BLy2 = float(input("Entre com o valor do vértice BLy do triangulo 2:"))
    TRx2 = float(input("Entre com o valor do vértice TRx do triangulo 2:"))
    TRy2 = float(input("Entre com o valor do vértice TRy do triangulo 2:"))

    rectOne = {"bottomLeft" : (BLx1,BLy1),
               "topRight"   : (TRx1,TRy1)
              }

    rectTwo = {"bottomLeft" : (BLx2,BLy2),
               "topRight"   : (TRx2,TRy2)
              }

    print(rectOne)
    print("------")
    print(rectTwo)
    if isCollisionDetected(rectOne, rectTwo):
        print("Colisão detectada")
    else:
        print("Colisão não detectada")

main()