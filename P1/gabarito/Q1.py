#Computacao I
#DEL - UFRJ
#2019.10.01 - PROVA 1 - QUESTÃO 1
#Juliano F Caldeira


from math import sqrt


def bhaskara_roots(a, b, c):
    delta = (b * b) - (4 * a * c)
    if delta < 0:
        print("r1,2 = " + str(-b / (2 * a)) + " +- " + str(sqrt(-delta) / (2 * a)) + " j")
        return 1
    else:
        print("r1 = " + str((-b + sqrt(delta) / (2 * a))) + " r2 = " + str((-b - sqrt(delta)) / (2 * a)))
    return 0

def main():
    a = int(input("Entre com a: "))
    b = int(input("Entre com b: "))
    c = int(input("Entre com c: "))
    result = bhaskara_roots(a, b, c)
    print("Resultado:" + str(result))

main()