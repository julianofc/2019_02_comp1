#Computacao I
#DEL - UFRJ
#2019.10.01 - PROVA 1 - QUESTÃO 4
#Juliano F Caldeira

def nob(num_a, num_b):

    if len(num_a) < 2:
        int_a = int(num_a)
    else:
        int_a = int(num_a[0])+int(num_a[1])

    if len(num_b) < 2:
        int_b = int(num_b)
    else:
        int_b = int(num_b[0])+int(num_b[1])

    return int_a + int_b


def main():
    #Teste para "?"
    m_a = "21"
    m_b = "36"
    print("Valor esperado para a '?' com entradas 21 e 36:")
    print("Valor do enigma de Nob:", nob(m_a,m_b))

    #Teste para qualquer input
    print("Agora escolha os seus dois valores de entrada e teste a saida:")
    m_a = input("Entre com o primeiro valor:")
    m_b = input("Entre com o segundo valor:")
    print("Valor do enigma de Nob:", nob(m_a,m_b))

main()