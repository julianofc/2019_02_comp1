#Computacao I
#DEL - UFRJ
#2019.10.01 - PROVA 1 - QUESTÃO 3
#Juliano F Caldeira

import math


def calcPI_recursive(numits):
    if numits == 1:
        return 4
    else:
        denominator = (numits * 2) - 1
        if numits % 2 == 0:
            return -(4/denominator) + calcPI_recursive(numits-1)
        else:
            return (4/denominator) + calcPI_recursive(numits-1)

def calcPI(numits):
    m_PI = 4
    while numits > 1:
        denominator = (numits * 2) - 1
        if numits % 2 == 0:
            m_PI -= (4/denominator)
        else:
            m_PI += (4/denominator)
        numits -= 1
    return m_PI


def main():
    iter = 1
    while abs(calcPI_recursive(iter+1)-calcPI_recursive(iter)) > 5e-3: # > que 10⁻3 não há stack suficiente
        iter += 1
    print("Valor de PI com 5x10e-3 (critério de parada) e algoritmo recursivo:")
    print("Número de iteracoes: ",iter)
    print("Valor de PI: ",calcPI_recursive(iter+1))
    print("///////////////////////////")
    iter = 1
    print("Valor de PI com 5x10e-4 (critério de parada) e algoritmo não otimizado:")
    while abs(calcPI(iter+1)-calcPI(iter)) > 5e-4: #10⁻8 fica muito tempo executando
        iter += 1
    print("Número de iteracoes: ",iter)
    print("Valor de PI: ",calcPI(iter+1))

main()