# =====================================
######## Questao 2 ########

numImpar = 0
somaImpar = 0
mediaImpar = 0
maximo = 0
valor = 0
num = 1

while (valor != -1):
	print('Entre com o ', num, 'o valor: ', sep='')		
	valor = int(input())
	if (valor % 2 == 0):
		if (valor % 10 != 2):
			if (valor > maximo):
				maximo = valor
	elif (valor != -1):
		numImpar += 1
		somaImpar += valor
	num += 1
if (numImpar):
	mediaImpar = somaImpar/numImpar
print('Numero de impares = ', numImpar)
print('Média de impares = ', mediaImpar)
print('Maior par não terminado por 2 = ', maximo)
