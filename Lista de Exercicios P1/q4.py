#=======================================
######## Questao 4 ########

def ordena(lista):
	size = len(lista)
	troquei = True
	cont = 1
	while (troquei):
		troquei = False
		for index in range(size - cont):
			if (lista[index] > lista[index + 1]):
				aux = lista[index]
				lista[index] = lista[index + 1]
				lista[index + 1] = aux
				troquei = True
		cont += 1
	return lista
def main():
	teste = [4, 6, 3, 8, 1, 5, 2, 7]
	testeOrdenado = ordena(teste.copy())
	print(teste)
	print(testeOrdenado)
main()
