######## Questão 1 ########
from math import sqrt
def raizes(a, b, c):
	delta = b*b - 4 * a * c
	if (delta >= 0):
		r1 = (-b + sqrt(delta))/(2*a)
		r2 = (-b - sqrt(delta))/(2*a)
		print("r1 = ", r1 ) 
		print("r2 = ", r2 ) 
		res = 0
	else:
		delta = -delta
		im = sqrt(delta)/(2*a)
		real = -b/(2*a)
		print("r1 = ", real, " + ", im, "i") 
		print("r2 = ", real, " - ", im, "i") 
		res = 1
	return res
def main():

	a = int(input('entre com a: '))
	b = int(input('entre com b: '))
	c = int(input('entre com c: '))
	resultado = raizes(a, b, c)
	print("Resultado:" + str(resultado))

main()
